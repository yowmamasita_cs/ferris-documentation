Tutorial: Multi-User Blog
=========================

.. toctree::
  :maxdepth: 1
  :hidden:
  :includehidden:

  2_data_models
  3_controllers
  4_scaffolding
  5_templates
  6_functional_testing
  7_extras


We're going to walk through creating a simple application that acts as a multi-user blog. This tutorial will introduce you to the core concepts of Ferris. After completing this tutorial, you should feel confident enough to start your own applications with the use of the material in the User's Guide.

The essential functionality of our blog will be:

 * Users will be able to create new posts.
 * Users will be able to see all of their posts by date order.
 * Users can see the posts of all other users by date order.

Please make sure you've completed all the steps in :doc:`../getting_started` and have an empty application up and running.
