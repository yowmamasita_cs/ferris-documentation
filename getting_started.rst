Getting Started
===============

Prerequisites
-------------

Ferris has very few dependencies because it relies on included App Engine libraries and bundles everything else. Just make sure you have the latest Google App Engine SDK for Python installed.

However, if you want take advantage of unit testing you'll need to install `ferrisnose <https://pypi.python.org/pypi/FerrisNose>`_ via `pip <http://www.pip-installer.org/en/latest/>`_::

    pip install --upgrade ferrisnose


.. note:: You may need to run this command as root (sudo).


Getting a copy of Ferris
------------------------

The latest version of Ferris can be always be obtained from `Bitbucket <https://bitbucket.org/cloudsherpas/ferris-framework>`_. You can either download a copy of the source or use git to clone Ferris. The easiest way for most is to just download a zip of the latest release (`direct link to download <https://bitbucket.org/cloudsherpas/ferris-framework/get/master.zip>`_). Either way you do it, place the contents of the Ferris' source into the directory where you will be creating your application.


Configuration
-------------

A little bit of configuration is required. Open ``./app.yaml`` and set the application and version properties appropriately, like below::

    application: ferris-getting-started    #.appspot.com
    version: 1

You'll want to pick a unique application name in case you want to actually deploy this. For more information, check out the `App Engine <https://developers.google.com/appengine/docs/python/config/appconfig>`_ documentation.

If you wish to use OAuth2 or Email, you'll also need to change some settings in ``./app/settings.py``.


Running with the App Engine development server
----------------------------------------------

Using the development server with a Ferris application is the same as using it with any other App Engine application. Just issue ``dev_appserver.py`` . on unix/linux or use the `launcher <https://developers.google.com/appengine/training/intro/gettingstarted#starting>`_ on Windows/Mac. Once it's started you should be able to open up your app via http://localhost:8080. You should see a rather generic landing page.

.. note::
    If you're using the launcher, the URL for your application and the App Engine console may use a different port. Make note of this as the tutorial and examples all use http://localhost:8080 and http://localhost:8000 for the application and console respectively. 

.. tip::
    If you're running on linux or mac and want to use the command line tools (which are sometimes less frustrating than the launcher) you can use our `app-server <https://bitbucket.org/cloudsherpas/ubuntu-environment-bootstrap/src/master/app-server.sh>`_ script. This script insures that your data is persisted between reboots and sets some useful default flags.


.. note::
    Windows (and sometimes OSX) can be weird and really inconsistent when setting up an environment. If you run into trouble feel free to reach out to us on the mailing list. We recommend Ubuntu (or another flavor of linux) for development as setting up the Google Cloud SDK on linux is dead easy. You can also use vagrant to run a headless ubuntu VM on Window or OSX- we even have an example `Vagrantfile <https://bitbucket.org/cloudsherpas/ubuntu-environment-bootstrap/src/master/Vagrantfile>`_ to get you going. 
